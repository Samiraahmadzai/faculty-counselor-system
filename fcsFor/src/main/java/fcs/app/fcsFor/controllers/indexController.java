package fcs.app.fcsFor.controllers;

import fcs.app.fcsFor.models.Faculty;
import fcs.app.fcsFor.models.Faculty_info;
import fcs.app.fcsFor.repositories.FacultyRepository;
import fcs.app.fcsFor.repositories.Faculty_infoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Controller
public class indexController {

    @Autowired
    private Faculty_infoRepository faculty_infoRepository;

    @Autowired
    private FacultyRepository facultyRepository;

    @RequestMapping("/index")
    public String index(Model model){
        model.addAttribute("faulties",facultyRepository.findAll());
//        model.addAttribute("faculty", facultyRepository.findById(1).get());
//        System.out.println(facultyRepository.findById(2).get().getName());
        return "index";

    }

    @GetMapping("/someendpoint")
    public @ResponseBody Set<Faculty_info> getId(@RequestParam("id") int id ){
//        Faculty faculty=new Faculty("Computer Science");
//        Faculty_info faculty_info=new Faculty_info("Introduction","This is the first faculty in the university","12.13.2019",faculty);
//
//        faculty_infoRepository.save(faculty_info);
        return facultyRepository.findById(id).get().getFaculty_infos();

//        return faculty_infos;
    }

}
