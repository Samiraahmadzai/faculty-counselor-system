package fcs.app.fcsFor.controllers;

import com.mysql.cj.xdevapi.JsonArray;
import fcs.app.fcsFor.models.Question;
import fcs.app.fcsFor.models.Subject;
import fcs.app.fcsFor.repositories.QuestionRepository;
import fcs.app.fcsFor.repositories.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Controller
public class testController {
    @Autowired
    private SubjectRepository subjectRepository;

    @RequestMapping("/test")
    public String test(Model model) {
        model.addAttribute("subjects", subjectRepository.findAll());
        return "test";
    }

    @Autowired
    private QuestionRepository questionRepository;


    @RequestMapping(value = "/subid", method = RequestMethod.POST)
    @ResponseBody
    public String idpassed(@RequestParam(value = "ids[]") List<Integer> ids) {
       List<Subject> s= (List<Subject>) subjectRepository.findAllById(ids);
        for (Subject i :s) {
            System.out.println(i.getQuestions());
        }
        return "done";
    }

}