package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Subject;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.criteria.CriteriaUpdate;

public interface SubjectRepository extends CrudRepository<Subject,Integer> {
}
