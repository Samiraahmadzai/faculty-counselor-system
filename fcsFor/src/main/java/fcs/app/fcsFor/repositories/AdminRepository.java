package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Admin;
import org.springframework.data.repository.CrudRepository;

public interface AdminRepository extends CrudRepository<Admin,Integer> {
}
