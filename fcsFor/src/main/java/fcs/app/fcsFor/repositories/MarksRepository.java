package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Marks;
import org.springframework.data.repository.CrudRepository;

public interface MarksRepository extends CrudRepository<Marks,Integer> {
}
