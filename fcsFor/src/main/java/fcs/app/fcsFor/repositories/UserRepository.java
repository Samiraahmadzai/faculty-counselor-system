package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Integer> {
}
