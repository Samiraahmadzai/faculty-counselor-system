package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment,Integer> {
}
