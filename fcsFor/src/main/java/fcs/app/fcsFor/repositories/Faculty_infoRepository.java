package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Faculty_info;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Faculty_infoRepository extends CrudRepository<Faculty_info,Integer> {
//        @Query("select * from faculty_info where faculty_id = id")
//    public List<Faculty_info>
}

