package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Recommendation;
import org.springframework.data.repository.CrudRepository;

public interface RecommendationRep extends CrudRepository<Recommendation,Integer> {
}
