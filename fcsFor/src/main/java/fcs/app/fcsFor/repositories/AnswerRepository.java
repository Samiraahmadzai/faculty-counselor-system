package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Answer;
import org.springframework.data.repository.CrudRepository;

public interface AnswerRepository extends CrudRepository<Answer,Integer> {
}
