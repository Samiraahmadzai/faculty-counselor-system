package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Question;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuestionRepository extends CrudRepository<Question,Integer> {


}
