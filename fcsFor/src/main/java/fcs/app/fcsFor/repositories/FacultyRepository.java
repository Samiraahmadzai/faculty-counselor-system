package fcs.app.fcsFor.repositories;

import fcs.app.fcsFor.models.Faculty;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FacultyRepository extends CrudRepository<Faculty,Integer> {
//    public List<Faculty> findByName(String name);
}
