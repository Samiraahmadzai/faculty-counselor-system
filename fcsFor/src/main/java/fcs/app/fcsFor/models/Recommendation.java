package fcs.app.fcsFor.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Recommendation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int r_id;
    private String first_recommendation;
    private String second_recommendation;
    private String third_recommendation;
    private int marks_id;

    public int getR_id() {
        return r_id;
    }

    public void setR_id(int r_id) {
        this.r_id = r_id;
    }

    public String getFirst_recommendation() {
        return first_recommendation;
    }

    public void setFirst_recommendation(String first_recommendation) {
        this.first_recommendation = first_recommendation;
    }

    public String getSecond_recommendation() {
        return second_recommendation;
    }

    public void setSecond_recommendation(String second_recommendation) {
        this.second_recommendation = second_recommendation;
    }

    public String getThird_recommendation() {
        return third_recommendation;
    }

    public void setThird_recommendation(String third_recommendation) {
        this.third_recommendation = third_recommendation;
    }

    public int getMarks_id() {
        return marks_id;
    }

    public void setMarks_id(int marks_id) {
        this.marks_id = marks_id;
    }
}
