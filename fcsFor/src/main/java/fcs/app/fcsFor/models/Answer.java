package fcs.app.fcsFor.models;

import javax.persistence.*;

@Entity
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int a_id;
    private String answer;
    private boolean correct_choice;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "q_id")
    private Question question;

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public int getA_id() {
        return a_id;
    }

    public void setA_id(int a_id) {
        this.a_id = a_id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isCorrect_choice() {
        return correct_choice;
    }

    public void setCorrect_choice(boolean correct_choice) {
        this.correct_choice = correct_choice;
    }

}
