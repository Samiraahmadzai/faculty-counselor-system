package fcs.app.fcsFor.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.engine.internal.Cascade;

import javax.persistence.*;
import java.util.Set;


//@NamedQuery(name = "Faculty.findByName", query="select faculty.id from faculty where LOWER(faculty.name) =LOWER(?1) ")
@Entity
public class Faculty {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;



    @OneToMany(fetch =FetchType.LAZY , mappedBy = "faculty", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("faculty")
    private Set<Faculty_info> faculty_infos;

    public Faculty() {
    }

    public Faculty(String name) {
        this.name = name;
    }

    public Set<Faculty_info> getFaculty_infos() {
        return faculty_infos;
    }

    public void setFaculty_infos(Set<Faculty_info> faculty_infos) {
        this.faculty_infos = faculty_infos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", faculty_infos=" + faculty_infos +
                '}';
    }
}
