-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2019 at 01:41 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fcs`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `allAnswers` ()  BEGIN
select * from answer;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user_name` varchar(60) COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `a_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `correct_choice` tinyint(1) NOT NULL,
  `q_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`a_id`, `answer`, `correct_choice`, `q_id`) VALUES
(1, '- 4', 0, 1),
(2, '4', 1, 1),
(3, '1 / 4 ', 0, 1),
(4, '10', 0, 1),
(5, '20', 0, 2),
(6, '4', 0, 2),
(7, ' 0.4 ', 1, 2),
(8, ' 00.4 ', 0, 2),
(9, '11', 0, 3),
(10, '48', 0, 3),
(11, ' -12 ', 0, 3),
(12, '22', 1, 3),
(13, '1 846 000 ', 1, 4),
(14, '1 852 000 ', 0, 4),
(15, '1 000 000 ', 0, 4),
(16, '1 500 000 ', 0, 4),
(17, '[ -5 , + infinity) ', 0, 5),
(18, '[ 2 , + infinity) ', 0, 5),
(19, ' ( - infinity , 2] ', 1, 5),
(20, '( - infinity , 0]', 0, 5),
(21, 'on the y axis ', 0, 6),
(22, 'on the x axis ', 1, 6),
(23, 'on the line y = x ', 0, 6),
(24, 'on the line y = - x ', 0, 6),
(25, 'A = a - 3, B = b ', 0, 7),
(26, 'A = a - 3, B = b ', 0, 7),
(27, 'A = a + 3, B = -2 b ', 0, 7),
(28, 'A = a + 3, B = -2 b +3', 1, 7),
(29, '(-5 , -1) ', 1, 8),
(30, '(-5 , -5) ', 0, 8),
(31, '(-1 , -3) ', 0, 8),
(32, ' (-2 , -3)', 0, 8),
(33, 'are parallel ', 0, 9),
(34, 'intersect at one point ', 0, 9),
(35, 'intersect at two points ', 0, 9),
(36, 'perpendicular', 1, 9),
(37, 'intersect at two points ', 0, 10),
(38, 'intersect at one point ', 0, 10),
(39, 'do not intersect ', 1, 10),
(40, 'none of the above', 0, 10),
(41, '(0 , Ï€/2) ', 0, 11),
(42, '(Ï€/2 , Ï€) ', 1, 11),
(43, '(Ï€ , 3 Ï€ / 2) ', 0, 11),
(44, ' (3 Ï€ / 2 , 2 Ï€)', 0, 11),
(45, '- 4, -2, and 1 ', 0, 12),
(46, '-2, 0 and 3 ', 0, 12),
(47, '4, 2, and 5 ', 0, 12),
(48, '0, 2 and 5', 1, 12),
(49, '- 2, 4, and 11/2 ', 1, 13),
(50, ' - 8, 16 and 22 ', 0, 13),
(51, '- 4, 8, and 11 ', 0, 13),
(52, '2, 19 / 2 and 7 / 2', 0, 13),
(53, '10', 0, 14),
(54, '15', 0, 14),
(55, '2100 ', 1, 14),
(56, '8', 0, 14),
(57, '5!', 0, 15),
(58, '3!', 1, 15),
(59, '2!', 0, 15),
(60, '3! * 2!', 0, 15),
(61, 'mean = 15 , standard deviation = 6 ', 0, 16),
(62, 'mean = 10 , standard deviation = 6 ', 0, 16),
(63, 'mean = 15 , standard deviation = 1 ', 1, 16),
(64, 'mean = 10 , standard deviation = 1', 0, 16),
(65, 'an odd function ', 0, 18),
(66, 'an even function ', 1, 18),
(67, 'neither odd nor even ', 0, 18),
(68, 'even and odd', 0, 18),
(69, '2 Ï€ ', 0, 19),
(70, '2 Ï€ / 3', 0, 19),
(71, 'Ï€ / 3 ', 1, 19),
(72, '3 Ï€ ', 0, 19),
(73, '1 cm ', 0, 20),
(74, '1.2 cm ', 1, 20),
(75, '2 cm ', 0, 20),
(76, '0.6 cm', 0, 20),
(77, '4 Ï€ 2', 0, 21),
(78, '2 Ï€ ', 0, 21),
(79, '4 Ï€ ', 0, 21),
(80, ' Ï€ ', 1, 21),
(81, '0.057 ', 1, 22),
(82, '0.478 ', 0, 22),
(83, '0.001 ', 0, 22),
(84, '0', 0, 22),
(85, 'Israfil', 0, 23),
(86, 'Kiraman Katibin', 1, 23),
(87, 'Mikail', 0, 23),
(88, 'Izrail', 0, 23),
(89, 'Qadr ul Islam', 0, 24),
(90, 'Arkanal Islam', 1, 24),
(91, 'Yusuf Islam', 0, 24),
(92, 'Fi Amanillah', 0, 24),
(93, ' Nabi', 1, 25),
(94, 'Rasul', 0, 25),
(95, 'Wahi', 0, 25),
(96, 'None of the above', 0, 25),
(97, 'Islam means to obey Allah (s.w.t.) and follow His commands', 1, 26),
(98, 'Islam means to obey our parents', 0, 26),
(99, 'Islam means to obey our teachers and elders', 0, 26),
(100, 'None of the above', 0, 26),
(101, ' Yarhamukallah', 0, 27),
(102, 'Ya Allah', 0, 27),
(103, 'Alhamdu lillah', 1, 27),
(104, 'La Ilaha Illal Lah', 0, 27),
(105, 'Muharram', 1, 28),
(106, 'Ramadan', 0, 28),
(107, 'Shawwal', 0, 28),
(108, 'Rabi-ul-Awal', 0, 28),
(109, 'English', 0, 29),
(110, 'Arabic', 1, 29),
(111, 'Urdu', 0, 29),
(112, 'Hebrew', 0, 29),
(113, 'The Most Knowledgable', 0, 30),
(114, 'The Most Powerful', 0, 30),
(115, 'The Most Merciful', 1, 30),
(116, 'All of the above', 0, 30),
(117, 'Nuh (a.s.)', 0, 31),
(118, 'Hud (a.s.)', 0, 31),
(119, 'Adam (a.s.)', 1, 31),
(120, 'Musa (a.s.)', 0, 31),
(121, 'Injil', 0, 32),
(122, 'Al-Quran', 1, 32),
(123, 'Tawrat', 0, 32),
(124, 'Zabur', 0, 32),
(125, ' Ihsan', 0, 33),
(126, 'Taqwa', 0, 33),
(127, ' Islam', 0, 33),
(128, ' Iman', 1, 33),
(129, 'Al-Ikhlas', 0, 35),
(130, 'Al-Fatihah', 1, 35),
(131, 'Al-Kawthar', 0, 35),
(132, 'The dawn', 0, 36),
(133, 'The opening', 0, 36),
(134, 'The people', 1, 36),
(135, 'The night', 0, 36),
(136, '23 years', 1, 37),
(137, '32 years', 0, 37),
(138, '40 years', 0, 37),
(139, '14 years', 0, 37),
(140, '23 years', 1, 38),
(141, '13 years', 0, 38),
(142, '40 years', 0, 38),
(143, '10 years', 0, 38),
(144, 'Saudi Arabia ', 0, 39),
(145, 'Indonesia ', 1, 39),
(146, 'Pakistan', 0, 39),
(147, 'Iraq ', 0, 39),
(148, 'Pray five times daily ', 0, 40),
(149, 'Profess Allah as the only God ', 0, 40),
(150, 'Make a pilgrimage to Mecca ', 0, 40),
(151, 'Read the Quran every day ', 1, 40),
(152, 'Injil', 0, 41),
(153, 'Zabur', 0, 41),
(154, 'Tawrat', 0, 41),
(155, 'Sahifah', 1, 41),
(156, 'Kitab', 0, 42),
(157, 'Jibrail', 0, 42),
(158, 'Injil', 0, 42),
(159, 'Wahi', 1, 42),
(160, 'A form of Muslim law ', 1, 43),
(161, 'A style of calligraphy', 0, 43),
(162, 'A native dance', 0, 43),
(163, 'A religious school ', 0, 43),
(164, '9/11', 1, 44),
(165, ' 7/11', 0, 44),
(166, '26/7', 0, 44),
(167, '7/7', 0, 44),
(168, 'Nadir Shah', 0, 45),
(169, 'Ahmed Shah Abdali', 0, 45),
(170, 'Mohammed Zahir Shah', 1, 45),
(171, 'Amanullah', 0, 45),
(172, 'Mohammed Dauod', 0, 46),
(173, 'Babrak Karmal', 1, 46),
(174, 'Noor Taraki', 1, 46),
(175, 'Rashid Dostum', 0, 46),
(176, ' Iran', 1, 47),
(177, 'Pakistan', 0, 47),
(178, 'Saudi Arabia', 0, 47),
(179, 'UAE', 0, 47),
(180, '1790-1792', 0, 48),
(181, '1799-1802', 0, 48),
(182, '1839-1842', 1, 48),
(183, '1878-1880', 0, 48),
(184, 'Nowshak', 1, 49),
(185, 'Safid Koh', 0, 49),
(186, 'Siah Band', 0, 49),
(187, 'Doshakh', 0, 49),
(188, 'Amu', 0, 50),
(189, 'Hilmand', 0, 50),
(190, 'Harirud', 0, 50),
(191, 'Kabul ', 1, 50),
(192, 'since 1996', 1, 51),
(193, 'since Afghanistan was created', 0, 51),
(194, '20 years', 0, 51),
(195, 'since WWII', 0, 51),
(196, 'Pakistan', 0, 53),
(197, 'England', 1, 53),
(198, 'Iran', 0, 53),
(199, 'Turkmenistan', 0, 53),
(200, '1971', 0, 54),
(201, '1975', 0, 54),
(202, '1979', 1, 54),
(203, '1981', 0, 54),
(204, 'Mujahideen', 1, 55),
(205, 'Hamas', 0, 55),
(206, 'Green Crescent', 0, 55),
(207, 'Al Qaeda', 0, 55),
(208, 'He was attracted by its wealth.', 0, 56),
(209, 'The Indian rulers were weak.', 0, 56),
(210, 'He had a strong army.', 1, 56),
(211, 'He was invited by Shah Nawaz Khan.', 0, 56),
(212, 'Iraq, Jordan, Syria, Turkey ', 0, 57),
(213, 'Iraq, Israel, Syria, Afghanistan, Republic of Armenia ', 0, 57),
(214, 'Iraq, Afghanistan, Kuwait, Turkey ', 0, 57),
(215, 'Iraq, Turkey, Afghanistan, Pakistan  ', 1, 57),
(216, 'Leonid Brezhnev', 1, 58),
(217, 'Nikita Khrushchev', 0, 58),
(218, 'Joseph Stalin', 0, 58),
(219, 'Mikhail Gorbachev', 0, 58),
(220, 'The Afghanis were fighting for their homeland', 0, 59),
(221, 'The mountains provided lots of good places to hide', 0, 59),
(222, 'The terrain was difficult and the Soviet army did not have the equipment for it', 0, 59),
(223, 'All of the above', 1, 59),
(224, '1975', 0, 60),
(225, '1982', 0, 60),
(226, '1989', 1, 60),
(227, '1992', 0, 60),
(228, 'George H. W. Bush and Ronald Reagan', 0, 61),
(229, 'Ronald Reagan and Jimmy Carter', 1, 61),
(230, '  Jimmy Carter and Gerald Ford', 0, 61),
(231, 'Gerald Ford and Richard Nixon', 0, 61),
(232, 'Leonid Brezhnev', 0, 62),
(233, ' Nikita Khrushchev', 0, 62),
(234, 'Vladimir Putin', 0, 62),
(235, 'Mikhail Gorbachev', 1, 62),
(236, 'Rudyard Kipling', 0, 63),
(237, 'George Orwell', 0, 63),
(238, 'Rabindranath Tagore', 1, 63),
(239, 'Sarojini Naidu', 0, 63),
(240, 'Durand line', 1, 64),
(241, 'McMohan line', 0, 64),
(242, 'Maginot line', 0, 64),
(243, 'Blue line', 0, 64),
(244, 'Northern Asia', 0, 65),
(245, 'Southern Asia ', 1, 65),
(246, 'Eastern Asia', 0, 65),
(247, 'Northeastern Asia', 0, 65),
(248, '89%', 0, 66),
(249, '64%', 0, 66),
(250, '23% ', 1, 66),
(251, '18%', 0, 66),
(252, 'About the same size as Rhode Island', 0, 67),
(253, 'Twice the size of New Jersey', 0, 67),
(254, 'Three times the size of Ohio', 0, 67),
(255, 'A little smaller than Texas', 1, 67),
(256, 'Kabul ', 1, 68),
(257, 'Kandahar ', 0, 68),
(258, 'Herat', 0, 68),
(259, 'Khost', 0, 68),
(260, 'Limited water, overgrazing, air and water pollution, and deforestation ', 1, 69),
(261, 'Overgrazing, poaching, air and water pollution, and deforestation', 0, 69),
(262, 'Desertification, poaching, overgrazing, and deforestation', 0, 69),
(263, 'Deforestation, desertification, limited water, and poaching', 0, 69),
(264, 'China, Nepal, Iran, and Pakistan', 1, 70),
(265, 'Iran, Pakistan, Tajikistan, and Turkmenistan ', 0, 70),
(266, 'Pakistan, Tajikistan, Turkmenistan, and Nepal', 0, 70),
(267, 'Turkmenistan, Uzbekistan, Nepal, and China', 0, 70),
(268, 'Earthquakes', 0, 71),
(269, 'Flooding', 0, 71),
(270, 'Droughts', 0, 71),
(271, 'All of the above', 1, 71),
(272, '2,568 km.', 0, 72),
(273, ' 987 km.', 0, 72),
(274, '243 km.', 0, 72),
(275, '0 km.', 1, 72),
(276, 'Tropical', 0, 74),
(277, 'Temperate to sub-arctic', 0, 74),
(278, 'Sub-arctic', 0, 74),
(279, 'Arid to semi-arid', 1, 74),
(280, '1986 ', 0, 75),
(281, '1992 ', 0, 75),
(282, '1988 ', 1, 75),
(283, '1994 ', 0, 75),
(284, '2001 ', 0, 76),
(285, '2003 ', 0, 76),
(286, '2015 ', 1, 76),
(287, '2017 ', 0, 76),
(288, 'Afghanistan  ', 0, 77),
(289, 'Pakistan ', 0, 77),
(290, 'Iran ', 1, 77),
(291, 'Between Afghanistan and Iran ', 0, 77),
(292, 'China ', 0, 78),
(293, 'India ', 0, 78),
(294, 'Iran ', 1, 78),
(295, 'Pakistan ', 0, 78),
(296, '1980 ', 0, 79),
(297, '1985 ', 1, 79),
(298, '1990 ', 0, 79),
(299, 'None ', 0, 79),
(300, 'Iran ', 0, 80),
(301, 'India ', 1, 80),
(302, 'Both ', 0, 80),
(303, 'None ', 0, 80),
(304, 'seismograph', 1, 81),
(305, 'quake meter', 0, 81),
(306, 'barometer', 0, 81),
(307, 'None of the above', 0, 81),
(308, 'Tobacco', 0, 82),
(309, 'Opium', 1, 82),
(310, 'Wheat', 0, 82),
(311, 'Cotton', 0, 82),
(312, 'Pacific Ocean', 0, 83),
(313, 'Atlantic Ocean', 0, 83),
(314, 'Indian Ocean', 1, 83),
(315, 'Arctic Ocean', 0, 83),
(316, 'South Eastern America', 0, 84),
(317, 'Australia', 0, 84),
(318, 'North  America', 0, 84),
(319, 'South Eastern Europe', 1, 84),
(320, 'At the equator', 1, 85),
(321, 'At the north pole', 0, 85),
(322, 'At the tropics', 0, 85),
(323, 'At the south pole', 0, 85),
(324, 'India and Pakistan', 1, 86),
(325, 'India and Afghanistan', 0, 86),
(326, 'India and China', 0, 86),
(327, 'India and Myanmar', 0, 86),
(328, 'USA', 0, 87),
(329, 'Brazil', 0, 87),
(330, 'Australia', 0, 87),
(331, 'Canada', 1, 87),
(332, 'erect', 0, 88),
(333, 'inverted', 1, 88),
(334, 'sometimes erect, sometimes inverted', 0, 88),
(335, 'none', 0, 88),
(336, 'Concave', 0, 89),
(337, 'Convex', 1, 89),
(338, 'Cylindrical', 0, 89),
(339, 'None', 0, 89),
(340, 'Excretion', 1, 90),
(341, 'Circulation', 0, 90),
(342, 'Reproduction', 0, 90),
(343, 'Pollution', 0, 90),
(344, 'meter', 0, 91),
(345, 'cm', 0, 91),
(346, 'watt', 0, 91),
(347, 'no unit', 1, 91),
(348, 'Bismuth', 0, 92),
(349, 'Magnesium', 0, 92),
(350, 'Mercury', 1, 92),
(351, 'Sodium', 0, 92),
(352, 'Red', 0, 93),
(353, 'Blue', 0, 93),
(354, 'Green', 0, 93),
(355, 'Yellow', 1, 93),
(356, 'Heat is evolved', 0, 94),
(357, 'Heat is absorbed', 1, 94),
(358, 'Temperature increases', 0, 94),
(359, 'Light is produced', 0, 94),
(360, 'Red', 1, 95),
(361, 'Blue', 0, 95),
(362, 'Violet', 0, 95),
(363, 'Green', 0, 95),
(364, 'Hydrochloric Acid', 1, 96),
(365, 'Citric Acid', 0, 96),
(366, 'Sulphuric Acid', 0, 96),
(367, 'Acetic Acid', 0, 96),
(368, 'WBC', 1, 97),
(369, 'Platelets', 0, 97),
(370, 'RBC', 0, 97),
(371, 'All of the above', 0, 97),
(372, 'ff', 1, 98),
(373, 'dd', 0, 98),
(374, 'ss', 0, 98),
(375, 'aa', 0, 98),
(376, '4', 0, 98),
(377, '6', 1, 98),
(378, '8', 0, 98),
(379, '10', 0, 98),
(380, '10 days', 0, 99),
(381, '120 days', 1, 99),
(382, '200 days', 0, 99),
(383, '360 days', 0, 99),
(384, 'phagocytosis (pron: fag-eh-seh-toe-sis)', 0, 100),
(385, 'hemolysis', 0, 100),
(386, 'mechanical damage', 0, 100),
(387, 'all of the above', 1, 100),
(388, 'pancreas ', 0, 101),
(389, 'spleen ', 1, 101),
(390, 'liver ', 0, 101),
(391, 'kidneys', 0, 101),
(392, '1 week', 0, 102),
(393, '3 weeks', 0, 102),
(394, '7 weeks', 1, 102),
(395, '21 weeks', 0, 102),
(396, 'erythrocytes (pron: eh-rith-row-cites)', 0, 103),
(397, 'leukocytes (pron: lew-kah-cites)', 1, 103),
(398, 'erythroblasts (pron: eh-rith-rah-blast)', 0, 103),
(399, 'thrombocytes (pron: throm-bow-cites)', 0, 103),
(400, 'leukocytosis (pron: lew-kO-sigh-toe-sis)', 0, 104),
(401, 'leukopenia (pron: lew-kO-pea-nee-ah)', 1, 104),
(402, 'leukemia (pron: lew-kee-me-ah)', 0, 104),
(403, 'leukohyperia (pron: lew-kO-high-per-e-ah)', 0, 104),
(404, 'white cells', 0, 105),
(405, 'red cells', 0, 105),
(406, 'platelets', 1, 105),
(407, 'erythrocytes', 0, 105),
(408, 'contain DNA', 1, 106),
(409, 'are roughly disk-shaped', 0, 106),
(410, 'have little ability to synthesize proteins', 0, 106),
(411, 'are between 1/2 and 1/3 the diameter of the red cell', 0, 106),
(412, 'medullary canal', 0, 107),
(413, 'cancellous bone', 0, 107),
(414, 'periosteum (pron: per-E-ahs-tee-em)', 0, 107),
(415, 'epiphysis (pron: eh-pif-eh-sis)', 1, 107),
(416, 'hypothalamus', 0, 108),
(417, 'midbrain', 1, 108),
(418, 'corpus callosum', 0, 108),
(419, 'cerebellum', 0, 108),
(420, 'frontal', 1, 109),
(421, 'parietal', 0, 109),
(422, 'temporal ', 0, 109),
(423, 'occipital', 0, 109),
(424, 'glial ', 0, 110),
(425, 'nodes of Ranvier (pron: ron-vee-ay)', 1, 110),
(426, 'collaterals', 0, 110),
(427, 'nodes of Babinet', 0, 110),
(428, 'human heart', 0, 111),
(429, 'tooth decay', 1, 111),
(430, 'kidneys ', 0, 111),
(431, 'liver', 0, 111),
(432, 'prophase (pron: prO-phase)', 0, 112),
(433, 'metaphase ', 0, 112),
(434, 'anaphase ', 0, 112),
(435, 'telophase', 1, 112),
(436, 'one', 0, 114),
(437, 'two', 1, 114),
(438, 'three', 0, 114),
(439, 'four', 0, 114),
(440, 'peptone and edestin', 0, 115),
(441, 'glutelin and leucine', 0, 115),
(442, 'valine and lysine', 0, 115),
(443, 'myosin and actin', 1, 115),
(444, 'paternal grandfather', 0, 116),
(445, 'grand mothers', 0, 116),
(446, 'maternal grandmother', 1, 116),
(447, 'synapse ', 1, 117),
(448, 'axon ', 0, 117),
(449, 'Nissl bodies', 0, 117),
(450, 'dendrite', 0, 117),
(451, 'lead and calcium ions', 0, 118),
(452, 'calcium and phosphate ions', 0, 118),
(453, 'sodium and potassium ions', 1, 118),
(454, 'potassium and phosphate ions', 0, 118),
(455, 'sensory ', 0, 119),
(456, 'motor ', 0, 119),
(457, 'association ', 0, 119),
(458, 'stimulatory', 1, 119),
(459, 'skin', 0, 120),
(460, 'pineal gland', 1, 120),
(461, 'liver ', 0, 120),
(462, 'pituitary gland', 0, 120),
(463, 'secreted by the pancreas', 0, 121),
(464, 'a protein', 0, 121),
(465, 'involved in the metabolism of glucose', 0, 121),
(466, 'all of the above', 1, 121),
(467, 'TSH - thyroid gland', 0, 122),
(468, 'ACTH - anterior pituitary', 0, 122),
(469, 'LH - ovary or testis', 0, 122),
(470, 'MSH - melanocytes (pron: meh-lan-o-cite)', 1, 122),
(471, 'acceleration and electric charge', 0, 123),
(472, 'mass and gravitational field', 0, 123),
(473, 'force and velocity', 1, 123),
(474, 'speed and time interval', 0, 123),
(475, 'move at a constant speed in a straight line', 1, 124),
(476, 'accelerate at a constant rate in a straight line', 0, 124),
(477, 'come to rest', 0, 124),
(478, 'increase its amount of inertia', 0, 124),
(479, '3.60 m/s2', 1, 125),
(480, '5.00 m/s2', 0, 125),
(481, '18.0 m/s2', 0, 125),
(482, '25.0 m/s2', 0, 125),
(483, 'increased electrical current and fewer coils of wire', 0, 126),
(484, ' increased diameter of coil and an increase in the number of coils', 0, 126),
(485, 'an increase in the electrical current and using a strong ferromagnetic material like iron', 1, 126),
(486, 'using an air core instead of iron and increasing the diameter of the coil ', 0, 126),
(487, '1.0 x 101 N', 0, 127),
(488, '37 N', 1, 127),
(489, '98 N', 0, 127),
(490, '360 N', 0, 127),
(491, '20. N', 0, 128),
(492, '29 N', 0, 128),
(493, '49 N', 0, 128),
(494, '69 N', 1, 128),
(495, '22.5 N', 0, 129),
(496, '37.5 N', 0, 129),
(497, '221 N', 0, 129),
(498, '368 N', 1, 129),
(499, 'strongest near a pole and the direction at any point in space is tangent to the field line', 1, 130),
(500, 'strongest far from a pole and the direction at any point in space is tangent to the field line', 0, 130),
(501, 'strongest near a pole and the direction at any point in space is perpendicular to the field line', 0, 130),
(502, 'strongest far from a pole and the direction at any point in space is perpendicular to the field line ', 0, 130),
(503, '2.00 m', 0, 131),
(504, '1.00 m', 0, 131),
(505, '0.500 m', 0, 131),
(506, '0.250 m', 1, 131),
(507, 'gravitational field line pattern for two neutral masses', 0, 132),
(508, 'electric field line pattern for two positive charges', 0, 132),
(509, 'electric field line pattern for two negative charges', 0, 132),
(510, 'electric field line pattern for two opposite charges', 1, 132),
(511, 'frequency decreases, the wavelength increases, and the speed increases', 0, 133),
(512, 'frequency does not change, the wavelength increases, and the speed increases', 1, 133),
(513, 'frequency does not change, the wavelength decreases, and the speed decreases', 0, 133),
(514, 'frequency does not change, the wavelength increases, and the speed decreases', 0, 133),
(515, 'magnetic north and geographic north', 0, 134),
(516, 'magnetic north and geographic south', 0, 134),
(517, 'the earthâ€™s magnetic field at any point and the vertical', 0, 134),
(518, 'the earthâ€™s magnetic field at any point and the horizontal ', 1, 134),
(519, 'depends on the speed of the wave', 0, 135),
(520, 'is the same for both waves', 0, 135),
(521, 'is greater for the 5,000 Hz wave', 1, 135),
(522, 'is greater for the 15,000 Hz wave', 0, 135),
(523, '64 Hz', 1, 136),
(524, '128 Hz', 0, 136),
(525, '252 Hz', 0, 136),
(526, '254 Hz', 0, 136),
(527, '20.6 m/s', 0, 137),
(528, '82.4 m/s', 0, 137),
(529, '3.30 x 102 m/s', 1, 137),
(530, '3.30 x 104 m/s', 0, 137),
(531, 'period will be the inverse of the frequency and the speed of the wave will be changed', 0, 138),
(532, 'period will be the inverse of the frequency and the speed of the wave will be constant', 1, 138),
(533, 'period will be identical to the frequency and the speed of the wave will be changed', 0, 138),
(534, 'period will be identical to the frequency and the speed of the wave will be constant', 0, 138),
(535, 'a standing wave pattern', 0, 139),
(536, 'destructive interference', 0, 139),
(537, 'constructive interference', 1, 139),
(538, 'a constant nodal point', 0, 139),
(539, '48.0 m', 0, 140),
(540, '24.0 m', 0, 140),
(541, '6.00 m', 1, 140),
(542, '3.00 m', 0, 140),
(543, '0.42 cm', 0, 141),
(544, '0.60 cm', 0, 141),
(545, '1.7 cm', 0, 141),
(546, '2.4 cm', 1, 141),
(547, '60 dB', 1, 142),
(548, '100 dB', 0, 142),
(549, '500 dB', 0, 142),
(550, '5000 dB', 0, 142),
(551, '2 Hz', 0, 143),
(552, '4 Hz', 1, 143),
(553, '8 Hz', 0, 143),
(554, '16 Hz', 0, 143),
(555, 'one-quarter the value for a tube open at both ends', 0, 144),
(556, 'one-half the value for a tube open at both ends', 1, 144),
(557, 'twice the value for a tube open at both ends', 0, 144),
(558, 'four times the value for a tube open at both ends', 0, 144),
(559, 'electrons and neutrons', 0, 145),
(560, 'electrons and protons', 0, 145),
(561, 'protons and neutrons', 1, 145),
(562, 'All of the above', 0, 145),
(563, 'molality', 1, 146),
(564, 'molarity', 0, 146),
(565, 'normality', 0, 146),
(566, 'formality', 0, 146),
(567, 'sodium', 0, 147),
(568, 'bromine', 0, 147),
(569, 'fluorine', 1, 147),
(570, 'oxygen', 0, 147),
(571, 'Na', 0, 148),
(572, 'Ag', 0, 148),
(573, 'Hg', 0, 148),
(574, 'Fe', 1, 148),
(575, 'p-electrons in Ne(Z = 10)', 0, 149),
(576, 's-electrons in Mg(Z = 12)', 0, 149),
(577, 'd-electrons in Fe(Z = 26)', 0, 149),
(578, 'p-electrons in CI(Z = 17)', 1, 149),
(579, 'smelting', 1, 150),
(580, 'roasting', 0, 150),
(581, 'calcinations', 0, 150),
(582, 'froth floatation', 0, 150),
(583, 'H2 at -73oC', 1, 151),
(584, 'CH4 at 300 K', 0, 151),
(585, 'N2 at 1,027oC', 0, 151),
(586, 'O2 at 0oC', 0, 151),
(587, 'H2CO3 - HCO3', 1, 154),
(588, 'H2CO3 - CO32-', 0, 154),
(589, 'CH3COOH - CH3COO-', 0, 154),
(590, 'NH2CONH2 - NH2CONH+', 0, 154),
(591, 'Alcohol', 0, 156),
(592, 'carbon dioxide', 0, 156),
(593, 'Chlorine', 1, 156),
(594, 'sodium chlorine', 0, 156),
(595, '1 proton only', 1, 157),
(596, '1 proton + 2 neutron', 0, 157),
(597, '1 neutron only', 0, 157),
(598, '1 electron only', 0, 157),
(599, 'specific heat', 0, 158),
(600, 'thermal capacity', 1, 158),
(601, 'water equivalent', 0, 158),
(602, 'None of the above', 0, 158),
(603, 'Electrons', 0, 159),
(604, 'Positrons', 0, 159),
(605, 'Neutrons', 0, 159),
(606, 'Mesons', 1, 159),
(607, '2.05 gram', 0, 160),
(608, '3.05 gram', 1, 160),
(609, '4.05 gram', 0, 160),
(610, '5.05 gram', 0, 160),
(611, '2-methyl octane', 0, 161),
(612, 'n-heptane', 1, 161),
(613, 'iso-octane', 0, 161),
(614, '3-methyl octane', 0, 161),
(615, 'Ni', 1, 162),
(616, 'Pb', 0, 162),
(617, 'Cu', 0, 162),
(618, 'Pt', 0, 162),
(619, 'He', 0, 163),
(620, 'Ne', 0, 163),
(621, 'Ar', 1, 163),
(622, 'Xe', 0, 163),
(623, 'this acid, in ancient times, was used to eliminate ant-hills', 0, 164),
(624, 'this corrosive acid is secreted by ants to drive away their enemies', 0, 164),
(625, 'this acid was first obtained by the distillation of ants', 1, 164),
(626, 'ants are attracted by the odour of this acid', 0, 164),
(627, 'Monazite', 1, 165),
(628, 'Fluorspar', 0, 165),
(629, 'Bauxite', 0, 165),
(630, 'Magnetite', 0, 165),
(631, 'RNA molecules', 0, 166),
(632, 'Nucleotides', 0, 166),
(633, 'DNA molecules', 1, 166),
(634, 'Enzymes', 0, 166),
(635, '30 KJ', 0, 167),
(636, '1 KJ', 0, 167),
(637, '39 KJ', 1, 167),
(638, '29 KJ', 0, 167),
(639, '0.5 mole', 1, 168),
(640, '0.2 mole', 0, 168),
(641, '0.4 mole', 0, 168),
(642, '0.25 mole', 0, 168);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `c_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `job` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `issue` varchar(100) NOT NULL,
  `comment` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`c_id`, `name`, `job`, `email`, `date`, `issue`, `comment`) VALUES
(8, 'ahmdd', 'farhgjh', '645475/56/65', 'samiraahmadzai44@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'ddddfwedwwwwwwwwwwwwwwwwwwwwwwww'),
(9, '', '', '', '', '', ''),
(10, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  '),
(11, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  '),
(12, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  '),
(13, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  '),
(14, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  '),
(15, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  '),
(16, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  '),
(17, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  '),
(18, 'ahmadzai', 'student', '645475/56/65``', 'sahiba@gmail.com', 'sfsafseaahgdkhgjdfdngj', 'jkwqjfdkjkfhfhjkfhjljkhf;fjhjhfjkhffjfhhh hfkj  ');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `name`) VALUES
(1, 'Kandahar University'),
(2, 'Agriculture'),
(3, 'Computer Science'),
(4, 'Engineering'),
(5, 'Education and Training'),
(6, 'Economic'),
(7, 'Journalism'),
(8, 'Law and Political Science'),
(9, 'Language and Literature'),
(10, 'Medicine'),
(11, 'Public Administration'),
(12, 'Sharia');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_info`
--

CREATE TABLE `faculty_info` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `description` text COLLATE utf8_persian_ci NOT NULL,
  `date` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `faculty_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `faculty_info`
--

INSERT INTO `faculty_info` (`id`, `title`, `description`, `date`, `faculty_id`) VALUES
(1, 'Introduction', 'A place to reach your goals. The goals that we had from our childhood. A place to change dreams to reality. The dreams that we and our families made together. \nHere intelligent and highly qualified teachers, quality curriculum, transparency and calm environment motivate students to work hard and pursue their studies. It has well-built premises with well-equipped classrooms, labs, libraries and eateries. The modern library on the campus encourages students learn much and develop fast. Students come from different parts of the country to acquire high quality education and knowledge. All teachers support students and guide them to get their goals. \nTeachers facilitate students with easy ways to perform academically in such a great place and give us the abilities to think and work independently, persevere and accomplish tasks.\nSamira Ahmadzai\nJunior Student – Faculty of Computer Science\n', '1397/10/12', 1),
(2, 'Introduction', 'Agricultural Faculty 1369 Kandahar University University was inaugurated in the scientific framework of the year, beginning with the (18) students and (8) teachers'' teachers. But at the same time, local education and administrative activities for a year ended the local conditions and disadvantages. In 1376, the University of Herat, Shah Mahmood (Bachelor of Science), was attempted by the Ministry of Higher Education to conduct the University of Higher Education, despite the difficult problems faced by the group''s students from the Bachelor of Arts (6) professional and vocational teachers. In the presence of students, 7 (7) students attain the first class and keep the faculty active. The presence of medicine, institutional, and full-time presence and its time-to-day faculty may enable faculty staff and civil servants, in the year 1380. It will be the first time to graduate from the branch to seven of the students, which is one Profile success was. Still depends agricultural college days fifths (730) students in seventeen (17) and night cycles of deprivation (227) graduates to serve in the country in three phases.', '1397/3/3', 2),
(3, 'Vision', 'Training professional cadres to standardize the agriculture and livestock sector across the country and at the grassroots.', '1397/3/5', 2),
(4, 'Mission', 'Provide academic, educational and research environment equipped with modern technology to provide secure service to the community for the promotion of agricultural sector.', '1397/7/5', 2),
(5, 'Goal', 'The Department of Agriculture employs academic, educational, and research methods for teaching high school teachers, which is helpful for national and regional agricultural development.', '1397/5/7', 2),
(6, 'Values', '• High-quality craftsmanship\r\n• Spirituality and patriotism\r\n• Transparency in actions\r\n', '1397/7/8', 2),
(7, 'Introduction', 'The Computer Science Faculty (June 2014) was established in June, which was coordinated with the cancer of the year (2014). And aim only at the following areas such as:\r\n• Databases & Information Systems\r\n• Software Engineering\r\n• Networking\r\nTo train computer experts. At present, computer science faculty will train students in a general section and will be formed in three sections based on the above three mentioned sections in the future, which will gradually graduate to the degree at the higher level. This academic center has eight active and dedicated teachers, out of which 4 master minders and the remaining 4 are currently studying at the University of various European universities and in the European universities. Since the establishment of the university There are 4 classes, around 400 (14 females, 386 males), students are studying, now due to short-term computer science faculty lacking their special building, the University of Kandahar''s Information Technology Located in the center of ITCQ. Students occupy the needs of all government, private and industrial sectors. There is a yearbook of curriculum, approved by the Ministry of Higher Education. Approximately 95% of academics such as academic books, libraries, examinations and others are by English language which are imported from other international development universities.\r\n', '1397/3/2', 3),
(8, 'Vision', 'The vision of the faculty of computer science at Kandahar University is to be recognized as one of the leading faculty in offering advance knowledge and research opportunities, and prepare graduates to be leaders in the field of information and communication technologies in the country.\r\nProviding opportunities for advanced education and research as a leading part of the Information Technology and Telecommunications Technology and professional cadres.', '1397/6/5', 3),
(9, 'Mission', 'The faculty of computer science strives to create and offer an inclusive educational environment for faculty, students and staff to foster integrity, creativity and development, conduct researches in collaboration with regional and international organizations, contribute the well being of the society to deliver community services effectively, a prepare skilled and professional capacities to solve real world problems.\r\nImprovement in healthy education, initial and physical development in the relevant part, to provide guidance for research with local and international academic institutions, to provide efficient services for community improvement.', '1396/7/5', 3),
(10, 'Goals', '• Capacity Building Programs\r\n• Providing modern and standard programs and materials approved by the level of national and international level\r\n• Improve infrastructure activities\r\n', '1397/5/23', 3),
(11, 'Introduction', 'The Engineering Faculty was established in Kandahar University in 2000, and is having four departments i.e. Civil Engineering, Water and Environmental Engineering, Energy Engineering and Architectural. Educational Curriculum for the Engineering Departments has been established in close co-ordination with the world class standard universities as follows:The Civil Engineering’s educational curriculum is established in close co-ordination with Kabul and Kansas State Universities.The Water and Environmental Engineering department’s educational curriculum is established in close co-ordination with the Asian Institute of Technology (AIT) Thailand.Moreover, the Energy Engineering department has valued with financial support of USWDP, technical support of Texas A & M and KandaharUniversity. The Kandahar University EnergyEngineeringDept. set up the educational curriculum, provision of educational material and expect an equipped laboratory in the relevant field by the end of the year 2018.Ninety-five percent of books, instructional material and assignments are in English Language, and currently 27 female students alike 898 male students, in total 925 students have been studying in the particular departments run by 29 permanent teachers. And out of the specified number of teachers 90% are Master’s Degree Holders from highly reputed international universities; as well five teachers are pursuing their Doctoral degree.Up to now in the previous 15 batches, eight hundred ninety-three engineers are qualified and offered for the services in Civil Engineering, Water and Environmental Engineering, and Energy Engineering Dept.', '1396/5/6', 4),
(12, 'Mission', 'Engineering faculty will be a leading institution in graduation of will-known engineers, conducting advance researches and services.', '1397/6/5', 4),
(13, 'Vision', 'Engineering Faculty Strives to achieve its Mission by enhancing its lecturers’ capacity, applying standard curriculum, using its laboratory, library with coordination of other sectorial departments.', '1397/3/4', 4),
(14, 'Significant', '• Respecting to entire Islamic and Afghan rules and holy beings.\r\n• Respecting to Afghan Government and Constitution.\r\n• Respecting to humanity and avoiding every kind of harassment.\r\n• Committed to its responsibility and society.\r\n• Strong commitment to rebuild the country and avoid every kind of corruption\r\n', '1397/6/4', 4),
(15, 'Academic Facilities', '• Research Laboratory (Soil and Concrete)\r\n• Energy Engineering Laboratory\r\n• Water Engineering Laboratory\r\n• Library and research center\r\n• Access to Informational sources and internet\r\n• New Text Books\r\n• Study rooms equipped with new technology\r\n• Usage of Practical survey in a wide area (Practicing of survey in various fields)\r\n• Meteorological station/center\r\n', '1397/11/24', 4),
(16, 'Introduction', 'Faculty of Education has been established in 2003 initially with only Mathematics department after Agriculture, Medicine and Engineering faculties respectively. In terms of student’s population, Faculty of Education is the leading one at the university level and currently it has around 1800 students who are taught by 64 faculty members. It has also an evening shift where 510 students are taught in various departments. Beside that, the in-service education facilities are available for the graduates of Teacher Training Institute. In Faculty of Education, students are enrolled via national entrance exam (Kankor exam).', '1397/4/13', 5),
(17, 'Vision', 'Bring strong reformed change of thoughts and improvement in the field of education.', '1397/10/4', 5),
(18, 'Mission', 'To Train professional and skilled teachers for the afghan community and the establishment of a strong training and teaching system.', '1397/4/14', 5),
(19, 'Goal', 'Training of well-educated and academic personalities who would be well known for their excellence on national and international skill and will be committed to the strengthen of Islamic and Afghan Culture.', '1397/5/7', 5),
(20, 'Core values', '• Respect and honor to national and Islamic values\r\n• Strong intention to the people’s intellectual and materialistic development\r\n• Respect to high status of human being and social life\r\n• Understanding of social responsibilities\r\n• Inspiring independent and high thinking\r\n• Developing and expanding our cultural traditions\r\n• Attention to the academic research and innovations\r\n• Professional and academic literacy\r\n• Team work and mutual cooperation\r\n• Letting future generation of the society feel the important responsibility of education.\r\n', '1397/5/13', 5),
(21, 'Introduction', 'Kandahar University Economics Faculty was established after Agriculture, Medical, Education and Sharia Faculty as per the needs of the south west region and it was officially included as one of the faculties of Kandahar University in 2010.In 2011, 117 students enrolled at the faculty for the first time through an entrance exam of the Ministry of Higher Education; two of them were female and the remaining 115 were male. With regard to valuable effort by the university leadership, it was possible to build a building for faculty of economics in 2012 which was a major concern for university of Kandahar. After the establishment of the Faculty, students have been continuously enrolled in the various departments and so far five terms of students have been gradated in order to serve their society and country. Furthermore, in 2013 night shift faculty was approved by Ministry of Higher Education, it had initially 55 students’ enrollment. Currently Economics Faculty has got two shifts students in two different departments which are National Economics and Management and Business Administration Departments while the rest of the departments are in process of capacity building. Based on current facilities, the faculty has provided the required learning facilities to students who are officially sent by Ministry of Higher Education. In addition, institute graduates (14th grade graduates) can also be enrolled to the second class of faculty after passing the Kankor Exam.Faculty of economics has 22 lecturers, out of which 10 master degree holders and 11 are pursuing master degree and 1 PhD degree abroad the country.   \r\nNow the faculty has the following departments:  \r\nManagement and Business Administration Department\r\nNational Economics Department \r\nEntrepreneurship Department\r\nFinance and Banking Department\r\nStatistics and Econometrics Department \r\n', '1397/5/23', 6),
(22, 'Vision', 'Faculty of Economics shall be a competitive faculty both locally and nationally by educating young professionals in related economic spheres.', '1397/11/30', 6),
(23, 'Mission', 'Faculty of Economics, having a standard curriculum, is training qualified and innovative students in National Economics, Statistics, Banking, Entrepreneurship and Management fields that better suits the needs of market and society.', '1397/3/3', 6),
(24, 'Objectives', 'Identifying the role of supply and demand in a market economy.\r\nAmplifying the Academic & administrative staff and students’ professional skills\r\nIdentifying the necessary conditions for market economies to function well.\r\nUnderstanding of the economic role of government policy and the Central Bank\r\nDefining and analyzing economic problems using algebraic and statistical methods.\r\n', '1398/1/5', 6),
(25, 'Core Values', '• Creation and Acquisition of Knowledge\r\n• Application of Knowledge\r\n• Professionalism\r\n• Congenial and supportive Academic Environment\r\n• Preservation and Dissemination of Knowledge\r\n• Search for truth\r\n• Intellectual Integrity\r\n• Free Exchange of Ideas\r\n• Lifelong Learning\r\n• Independent Thought\r\n• Research Center\r\n', '1397/11/24', 6),
(26, 'Introduction', 'Found the Journalism and Public Relations Faculty Foundation (1391), which is currently a journalism and public relations department. Journalism and Public Relations Faculty (10) have pastoral members, two people in China Masters are engaged in the study and in this college (411) students are studying, out of which seven are female girls. Journalism and Public Relations Faculty (153) students in two (2) visits Has graduated from two departments (journalism and public relations) to serve the country and (1396-1397) in the seminar of the seminar (150) through the concert to the college The Faculty is a supervisory committee for the management and management of affairs, the Board of Education Studies, Quality Evolution, Curriculum, Environmental Protection, Management and Design, Broadcasting, Electronic Studies and Teaching Process. Kandahar university media operational center, university broadcasting and publication in the training facilities.', '1397/5/13', 7),
(27, 'Introduction', 'Found the Journalism and Public Relations Faculty Foundation (1391), which is currently a journalism and public relations department. Journalism and Public Relations Faculty (10) have pastoral members, two people in China Masters are engaged in the study and in this college (411) students are studying, out of which seven are female girls. Journalism and Public Relations Faculty (153) students in two (2) visits Has graduated from two departments (journalism and public relations) to serve the country and (1396-1397) in the seminar of the seminar (150) through the concert to the college The Faculty is a supervisory committee for the management and management of affairs, the Board of Education Studies, Quality Evolution, Curriculum, Environmental Protection, Management and Design, Broadcasting, Electronic Studies and Teaching Process. Kandahar university media operational center, university broadcasting and publication in the training facilities.', '1397/3/3', 7),
(28, 'Vision', 'Training of Islamic, professional and nationally-trained journalists at the level of journalism and public relations at the surface of the country and country.', '1397/10/4', 7),
(29, 'Mission', 'Journalism and Public Relations Faculty provides professional skills, research metaphysics and modern technology in the light of national strategy and ethical standards to the soccer journalists.', '1396/7/5', 7),
(30, 'Core Values', '• Strong fight for promotion of Islamic culture.\r\n• Reform and build public thoughts.\r\n• Feeling, honesty and trust.\r\n• Moral values and loyalty.\r\n', '1397/6/5', 7),
(31, 'Objectives', '• Enhance academic principles\r\n• Teaching teachers and students with the research methodology\r\n• Providing loyalty to Islamic and national values and development of the country\r\n• Writing analytical analysts to investigate secret facts in the community\r\n', '1397/11/24', 7),
(32, 'Introduction', 'The University of Kandahar established the Law and Political Sciences Faculty with two educational departments (Judges and Prosecutors and Administrative and Diplomacy). The Law and Political Sciences (10) faculty has formal teachers, Of course, two of them are masters of Masters and Masters Degrees in five. The Law and Political Sciences (460) are students, of which 447 are male and female (13). From the Law and Political Sciences Faculty (65) students in the first round (1396 AD), graduated and served to the community to serve as a female.', '1397/3/3', 8),
(33, 'Vision', 'The University of Law and Political Science will be the most prestigious universities in the training of political, institutional, judicial and judicial cadres.', '1397/5/23', 8),
(34, 'Mission', 'The Universities of Law and Political Studies are trying to provide community-based, academic, judicial, and judicial aspects to the community by providing standard academic environment.', '1397/7/8', 8),
(35, 'Core Values', '• Identification of Islam''s legal system and statutory laws\r\n• Principles and conditions\r\n• Ethics and justice\r\n• Transparency and sincerity\r\n• Principles and actions\r\n• Capacity and accounting\r\n', '1397/5/7', 8),
(36, 'Objectives', '• Providing commitment to community in political, administrative, judicial and judicial sectors.\r\n• Serving services at national level in legal and political fields.\r\n• Strengthening legal and institutional sections in the country.\r\n• To make any bilateral relations with national, regional and international universities, research centers, as well as government and non-governmental organizations, and promoting assistance in these fields.\r\n• Provide qualified cadres in diplomatic fields.\r\n• train cadres to fill the national level in the political, administrative, judicial and judicial sectors.\r\n• Probably qualified to investigate the purpose of the nation.\r\n', '1396/7/5', 8),
(37, 'Introduction', 'Faculty of languages and Literature (FLL), located in the southern area of Kandahar University (KDRU) is officially established in September, 2014 in the framework of Ministry of Higher Education (MoHE). FLL is positioned in a new building containing three floors with more than eight classrooms. It has a wonderful lecture hall which can have more than one hundred students at once. It also has rooms for conference, resource, quality assurance, research center. Structurally, FLL is having a dean, deputy of dean, educational manager, executive, peon and a watchman. FLL as other Faculties of the KDRU, has memberships in all the committees such as Leadership, Academic, Quality Assurance, Research, Publication, Curricula, and Environmental Protection Committees. FLL for the first time inaugurated two departments: Departments of Pashto and English languages and Literature. The former has six lecturers among whom two have been completing their Master Degrees in their professional fields inside the country. And the later has four lecturers, one of whom is pursuing his Master Degree in the country too. Strategically, FLL is planning to establish new departments not only for National languages and literature such as Dari, Balochi, and etc. but also for international languages and literature such as Arabic, Hindi, Chinese, Germany, French and Spanish. Academically, All FLL lecturers strive to complete their Mater Degrees and even the PhD programs in the country or abroad soon. FLL graduates students in the course of four years (eight semesters) based on Credit System institutionalized by Mohe with the award degree of Bachelor of Arts (B.A).', '1397/7/8', 9),
(38, 'Vision', 'Faculty of Languages and Literature will be a renowned faculty in the area of research, creation and translation in different languages and literature around Afghanistan, region and world.', '1397/11/24', 9),
(39, 'Mission', 'To train academic linguistics, writers and researchers in various languages of the world in view of national and international criteria to work consistently and responsibly to the country and humanity.', '1397/6/4', 9),
(40, 'Goals', '• Training creative writers and linguists in national, regional and international languages.\r\n• Producing artistic and creative works in various languages around the country, region and world.\r\n• Developing native language in arts and literature.\r\n', '1397/11/27', 9),
(41, 'Core Values', '• Respecting all Islamic, humanistic, sacred, and national values;\r\n• Training such professional researchers in various languages in order to enlighten the darken aspects of native languages;\r\n• Training such professional literature and literary men on the national, regional and international level with the aim of aestheticizing the monstrosities of the societies by means of artistic writings;\r\n• Undertaking all the activities according to the rules and regulation;\r\n• Considering academic and professional criteria.\r\n', '1398/1/5', 9),
(42, 'Introduction', 'Kandahar University Medical Faculty in 1372 In the southwestern country of Herat, the establishment of medical practitioner at the University of Kandahar has been established as a state-owned medical institute. Counting a number of young medical faculties at the country level, which is currently under development and development. Currently, around 400 students are studying from different provinces and from Pakistan''s country in Pakistan. Since 29th medical practitioners are taught by pedestrians. Since 1994, at the time of medical faculty, more than 500 (20 women) have been present at MD at the age of 13, to the doctors and communities, to help reduce the community. It. Most of them have been involved at the Mirwais Restaurant Hospital in Kandahar province, across the 34 provinces of the country and even outside of the country, other health facilities have been employed or in higher levels of education. There is no university or university job only Teaching, but also servicing academic research and community in general. Despite the underlying wars, fewer possibilities, and existing challenges, medical faculty has opened its scientific widespread manner and poses positive to the needs of the community (including health studies, investigations, and health services) in the medical field. Answer me.', '1397/3/30', 10),
(43, 'Vision', 'As a leading guide to medical training and training in Afghanistan, emerges as a training institute.', '1397/10/14', 10),
(44, 'Mission', 'Improve health conditions by using medicinal medicine and techniques, basic medical education, research, and social services.', '1397/7/18', 10),
(45, 'Objectives', '• Increasing acne and quality of medical professionals, in the area of dialysis at dialysis levels, which also occur in public health. Presentative medical education\r\n• Identifying health problems by identifying and investigating research and research in the health sector and finding solutions to them.\r\n• To conduct quality health services to the community by using modern advanced medicine methods\r\n• By observing social needs and learning lessons, knowledgeable, and committed people who are motivated to follow the characteristics of the young people who are forced to pursue Kandahar Medical Faculty:\r\n• Response to social health needs.\r\n• Assist public service to community.\r\n• Take a healthy role in continuous renewal and development of our daily system.\r\n• Ability to overcome and eliminate against new challenges\r\n \r\n', '1398/1/26', 10),
(46, 'Introduction', 'Kandahar University, in the year 2012 established another vital academic discipline (the faculty of Public Administration and policy) as a part of ministry of higher education & KDR University’s continuous & vigorous struggles for the improvement & access of formal education to the youth with the support of German Federal government.', '1396/7/15', 11),
(47, 'Vision', 'To become one of the best and leading faculty in the country by developing good leaders, policy makers and professional managers who will lead the country for a better and brighter future.', '1397/10/14', 11),
(48, 'Mission', 'Training and development of youth with the help of new technology and effective teaching methods, doing research and taking active part in the improvement of public services delivery.', '1397/3/23', 11),
(49, 'Objectives', '• Training and developing youth with leadership and managerial skills\r\n• Doing research for the resolution of society’s problems\r\n• Providing consultations to the government for good governance\r\n', '1397/5/2', 11),
(50, 'Core Values', '• Promoting responsiveness, transparency, responsibility and accountability\r\n• Cultivating integrity, Fairness and equality\r\n• Enhancing mutual respect, \r\n• Developing internal open communication (vertical and horizontal)\r\n• Developing good relationships with both national and international institutions for the enrichment of PA faculty\r\n• Encouraging active participation of employees in decision making\r\n• Strictly following faculty’ strategic plan\r\n• Obeying Public Administration & Policy faculty’s rules and regulations\r\n• Adhering to the overall rules and regulations of KDR university and ministry of higher education\r\n', '1397/7/18', 11),
(51, 'Introduction', 'Sharia Faculty was founded at Kandahar University in 2009. During the first year, its strength of students was 70 in two different departments (Jurisprudence & Law and Islamic studies). In that time, it had three cadres and a few contract lecturers. From 2010 to 2012 after tolerating a lot of patience, it was able to find nine cadres and 11 contract lecturers, and the number of students reached to 277. In addition, it inaugurated the Islamic studies department of night shift in 2012 which admitted 73 students. Thus, the number of both shifts’ students reached to 350.\r\nIn 2013, it also opened exclusive Islamic studies department for girls which still keep working hard and sincerely.\r\nThe current statistic of Sharia Faculty’s lecturers, administrative staff and students is as below:\r\n1- Academic cadres: 21, five of which are teaching assistants and the rest of them are junior teaching assistants.\r\n2- Administrative staff: 8 members: one dean, one vice- dean, two administrators (executive and teaching) and four employees.\r\n', '1396/7/29', 12),
(52, 'Vision', 'It aspires to educate expert judges, lawyers, preachers and religious scholars in order to lead the country’s justice and legal system in accordance to modern and developing standards of the time, and to achieve the justice for all in the country. Furthermore, they will strive to direct the people to brotherhood, mutual respect, and justice and combined relaxed life. They will also strive to make the people aware of the Islam and its values.', '1398/1/5', 12),
(53, 'Mission', 'Sharia faculty considers the rehabilitation of the country and development of the society in the religious, academic, justice and legal fields its responsibility and mission.', '1397/2/8', 12),
(54, 'Some of the Sharia Faculty mission’s essential points are as bellow:', 'It fosters active participation in the rehabilitation and development of the Islamic Republic of Afghanistan.\r\nPresenting the experts and specialists in justice, jurisprudence, codification and missionary work to the society.\r\n Working hard as an active and strong religious source in the region.\r\nAttracting big number of youth to the faculty from the region.\r\nCombating against literacy and unemployment in the region.\r\nEstablishing ties and partnership with Islamic nations through close relationship with the international Islamic universities.\r\nContributing actively in restoring the international Islamic civilization on the behalf of our nation.\r\n', '1397/6/25', 12),
(55, 'Goal', 'Its major goal is to present the expert and knowledgeable religious scholars, judges, prosecutors, defense lawyer, lawyers, prayer leaders, preachers and teachers to the society in accordance to Islamic principles along with taking in consideration the Afghans’ sociology and international developments in order to be able to serve the country, religion and play central role in the solution of the current issues and problems. Finally, the graduates will help the Afghan nation with the awareness of vital Islam- related matters.', '1397/4/3', 12);

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `m_id` int(11) NOT NULL,
  `marks` int(11) NOT NULL,
  `percentage` float NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `q_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `q_points` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`q_id`, `question`, `q_points`, `subject_id`) VALUES
(1, 'If Logx (1 / 8) = - 3 / 2, then x is equal to ', 4, 1),
(2, '20 % of 2 is equal to ', 2, 1),
(3, 'If Log 4 (x) = 12, then log 2 (x / 4) is equal to ', 4, 1),
(4, 'The population of a country increased by an average of 2% per year from 2000 to 2003. If the population of this country was 2 000 000 on December 31, 2003, then the population of this country on January 1, 2000, to the nearest thousand would have been ', 4, 1),
(5, 'f is a quadratic function whose graph is a parabola opening upward and has a vertex on the x-axis. The graph of the new function g defined by g(x) = 2 - f(x - 5) has a range defined by the interval ', 4, 1),
(6, 'f is a function such that f(x) < 0. The graph of the new function g defined by g(x) = | f(x) | is a reflection of the graph of f ', 4, 1),
(7, 'If the graph of y = f(x) is transformed into the graph of 2y - 6 = - 4 f(x - 3), point (a , b) on the graph of y = f(x) becomes point (A , B) on the graph of 2y - 6 = - 4 f(x - 3) where A and B are given by ', 4, 1),
(8, 'When a parabola represented by the equation y - 2x 2 = 8 x + 5 is translated 3 units to the left and 2 units up, the new parabola has its vertex at ', 4, 1),
(9, 'The graphs of the two linear equations a x + b y = c and b x - a y = c, where a, b and c are all not equal to zero, ', 4, 1),
(10, 'The graphs of the two equations y = a x 2 + b x + c and y = A x 2 + B x + C, such that a and A have different signs and that the quantities b 2 - 4 a c and B 2 - 4 A C are both negative, ', 4, 1),
(11, 'For x greater than or equal to zero and less than or equal to 2 Ï€, sin x and cos x are both decreasing on the intervals ', 4, 1),
(12, 'The three solutions of the equation f(x) = 0 are -2, 0, and 3. Therefore, the three solutions of the equation f(x - 2) = 0 are ', 4, 1),
(13, 'The three solutions of the equation f(x) = 0 are - 4, 8, and 11. Therefore, the three solutions of the equation f(2 x) = 0 are ', 4, 1),
(14, 'A school committee consists of 2 teachers and 4 students. The number of different committees that can be formed from 5 teachers and 10 students is ', 2, 1),
(15, 'Five different books (A, B, C, D and E) are to be arranged on a shelf. Books C and D are to be arranged first and second starting from the right of the shelf. The number of different orders in which books A, B and E may be arranged is ', 4, 1),
(16, 'The mean of a data set is equal to 10 and its standard deviation is equal to 1. If we add 5 to each data value, then the mean and standard deviation become ', 4, 1),
(18, 'If f(x) is an odd function, then | f(x) | is ', 4, 1),
(19, 'The period of | sin (3x) | is ', 4, 1),
(20, 'When a metallic ball bearing is placed inside a cylindrical container, of radius 2 cm, the height of the water, inside the container, increases by 0.6 cm. The radius, to the nearest tenth of a centimeter, of the ball bearing is ', 2, 1),
(21, 'The period of 2 sin x cos x is ', 2, 1),
(22, 'The probability that an electronic device produced by a company does not function properly is equal to 0.1. If 10 devices are bought, then the probability, to the nearest thousandth, that 7 devices function properly is ', 4, 1),
(23, 'What do we call the Angels who write down what we do ?', 3, 5),
(24, 'Pillars of Islam are also called ...â€¦', 2, 5),
(25, 'A prophet is called .......... in Arabic.', 2, 5),
(26, 'What does Islam mean ?', 3, 5),
(27, 'What do you say when you sneeze ?', 2, 5),
(28, 'What is the first month of the Islamic Calendar?', 3, 5),
(29, 'What is the language of the Quran?', 2, 5),
(30, 'Al-Rahman means ..â€¦', 2, 5),
(31, 'Who was the first prophet of Allah (s.w.t.)?', 2, 5),
(32, ' What is the name of the book sent down to Prophet Muhammad (s.a.w.)?', 2, 5),
(33, 'Believing in Allah (s.w.t.) , in His angels, in His books, in His messengers, in the last day, in destiny and in life after death means a Muslim has â€¦', 3, 5),
(35, '"You alone we worship and you alone do we ask for help" is a phrase from which Surah ?', 4, 5),
(36, 'What is the meaning of An-Nas ?', 2, 5),
(37, 'For how many years did Prophet Muhammad (s.a.w.) preach?', 2, 5),
(38, 'How long did it take to complete the revelation of the Quran?', 3, 5),
(39, 'What is the most populous Muslim country in the world? ', 3, 5),
(40, 'What is not one of the five pillars of Islam?', 2, 5),
(41, 'What is the name of the book sent down to Prophet Ibrahim (a.s.)?', 2, 5),
(42, 'What is the revelation from Allah (s.w.t.) is called in Arabic ?', 2, 5),
(43, 'What is the Shari’ah? ', 2, 5),
(44, 'How is the event that led to the invasion of Afghanistan by USA and its allies is known?', 2, 6),
(45, 'Who was deposed in 1973?', 2, 6),
(46, 'Who was installed as President of Afghanistan in 1979 after invasion by USSR?', 4, 6),
(47, ' Which of the following countries did not recognize the Taliban regime?', 2, 6),
(48, 'When was the first Anglo-Afghan War?', 4, 6),
(49, 'Which is the highest mountain in Afghanistan?', 3, 6),
(50, 'Which river in Afghanistan joins the Indus in Pakistan?', 2, 6),
(51, ' 	How long has the Taliban controlled Afghanistan?', 4, 6),
(53, '	Which of these countries does NOT border Afghanistan?', 2, 6),
(54, 'In which year did Soviet troops move into the Asian nation of Afghanistan, following political instability there?', 4, 6),
(55, 'What name was adopted by Islamic "freedom fighters" who resisted the Soviet occupation of Afghanistan in the 1980s?', 3, 6),
(56, ' The immediate cause of invasion of Ahmad Shah Abdali on India was', 3, 6),
(57, ' List all of the countries whose borders are contiguous with Iran: ', 3, 6),
(58, ' Who was president of the Soviet Union at the start of the Afghanistan War?', 4, 6),
(59, 'What made the war difficult for the Soviet Union army?', 4, 6),
(60, 'When did the Soviet Afghanistan War end?', 4, 6),
(61, 'Who were the two presidents of the United States during the Soviet Afghanistan War?', 4, 6),
(62, ' Who was the Soviet leader who put an end to the war?', 4, 6),
(63, 'Who wrote the story â€œThe Kabuliwallaâ€?', 4, 6),
(64, 'Which line separates Afghanistan from Pakistan?', 2, 6),
(65, 'Where is Afghanistan located?', 2, 7),
(66, 'What percentage of the population of Afghanistan lives in towns or cities?', 3, 7),
(67, ' How big is Afghanistan?', 4, 7),
(68, ' What is the capital of Afghanistan?', 2, 7),
(69, ' What are some current environmental issues in Afghanistan?', 4, 7),
(70, 'What are some countries that border Afghanistan?', 3, 7),
(71, ' Which of these are natural hazards in Afghanistan?', 4, 7),
(72, 'How many kilometres of coastline does Afghanistan have?', 4, 7),
(74, 'What kind of climate does Afghanistan have?', 3, 7),
(75, 'When did Afghanistanâ€™s first astronaut go to spac', 4, 7),
(76, 'When did Afghanistan get full mem', 3, 7),
(77, 'Chabahar port is located in which of the following countries? ', 4, 7),
(78, 'Which of the following countries have the worldâ€™s second largest gas reserves after Russia? ', 3, 7),
(79, 'The South Asian Association for Regional Cooperation was founded in: ', 3, 7),
(80, 'The  TAPI  project  is  a  natural  gas  pipeline  being  developed  between  Turkmenistan,  Afghanistan,  Pakistan and â€¦ ', 3, 7),
(81, 'Instrument used to measure earthquake is known as', 4, 7),
(82, 'The "Helmand province" of Afghanistan is famous for cultivation of', 3, 7),
(83, 'The "Ninety East Ridge" is a submarine volcanic ridge located in ', 4, 7),
(84, 'Black sea is sea in?', 3, 7),
(85, 'where do you find day and night equal?', 3, 7),
(86, 'The Radcliffe line is a boundary between?', 3, 7),
(87, 'Which country has the largest coast line?', 4, 7),
(88, 'The image formed in a compound microscope is', 4, 8),
(89, 'The lens used in a simple microscope is', 4, 8),
(90, 'The elimination of toxic nitrogenous waste and excess water in man is', 4, 8),
(91, 'The S. I. unit of refractive index is', 4, 8),
(92, 'The liquid metal is', 4, 8),
(93, 'Which of the following is not a primary color?', 4, 8),
(94, 'Endothermic reactions are those in which', 4, 8),
(95, 'Which color of light is deviated least', 4, 8),
(96, 'Acid present in gastric juice is', 4, 8),
(97, 'Which blood cells are called ''Soldiers'' of the body', 4, 8),
(98, ' The adult human of average age and size has approximately how many quarts of blood? Is it:', 3, 3),
(99, 'Once the erythrocytes enter the blood in humans, it is estimated that they have an average lifetime of how many days. Is it:', 3, 3),
(100, ' Of the following, which mechanisms are important in the death of erythrocytes (pron: eh-rith-reh-sites) in human blood? Is it', 4, 3),
(101, 'Surplus red blood cells, needed to meet an emergency, are MAINLY stored in what organ of the human body? Is it the:', 3, 3),
(102, 'When a human donor gives a pint of blood, it usually requires how many weeks for the body RESERVE of red corpuscles to be replaced? Is it:', 3, 3),
(103, 'The several types of white blood cells are sometime collectively referred to as:', 4, 3),
(104, 'The condition in which there is a DECREASE in the number of white blood cells in humans is known as:', 3, 3),
(105, 'The smallest of the FORMED elements of the blood are the:', 3, 3),
(106, 'Which of the following statements concerning platelets is INCORRECT. Platelets:', 4, 3),
(107, 'Lengthening of long bones in humans occurs in a particular area of the bone. This area is called the:', 4, 3),
(108, 'The part of the human brain which is an important relay station for the sensory impulses and also is the origin of many of the involuntary acts of the eye such as the narrowing of the pupil in bright light is the:', 4, 3),
(109, 'In which cerebral lobes is the speech center located? Is it the:', 4, 3),
(110, 'In most axons, the myelin sheath is interrupted at intervals of about 1 millimeter or more. These interruptions are called the:', 4, 3),
(111, 'Cariology is the study of the:', 3, 3),
(112, 'During the final stage of cell division, the mitotic apparatus disappears, the chromosomes become attenuated, the centrioles duplicate and split, the nuclear membrane becomes reconstituted and the nucleolus reappears. This phase of cell division is known as:', 4, 3),
(114, 'In most species of Paramecium there are how many contractile vacuoles? Is it:', 3, 3),
(115, 'The major fibrous proteins are:', 3, 3),
(116, 'From which grandparent or grandparents did you inherit your mitochondria (pron: my-toe-chon-dria)? Is it your:', 3, 3),
(117, 'Which of the following are NOT part of a neuron?', 3, 3),
(118, 'The resting potential of a neuron is dependent on what two ions?', 4, 3),
(119, 'Which of the following is NOT a type of neuron?', 3, 3),
(120, 'Melatonin (pron: mel-eh-toe-nin) is produced by the:', 4, 3),
(121, 'Which of the following statements is TRUE of insulin? Is it:', 4, 3),
(122, 'Select the hormone INCORRECTLY paired with its target.', 4, 3),
(123, 'Those two quantities that are vectors are', 3, 2),
(124, 'If an object is already moving and the sum of all the vector forces on a mass is zero, then the object will', 3, 2),
(125, 'Two forces are acting on a 5.00 kg mass. One of the forces is 10.0 N south and the other is 15.0 N east. The magnitude of the acceleration of the mass is ', 4, 2),
(126, 'The factors, both of which will increase the magnetic field strength of an electromagnet, are', 3, 2),
(127, 'On Mars, the gravitational field strength is 3.7 N/kg. An object has a weight of 98 N on the earth. The weight of this object on Mars is', 4, 2),
(128, 'A 5.0 kg mass is placed in an elevator that is accelerating upwards at 4.0 m/s2. The apparent weight of this mass is', 4, 2),
(129, 'A 50.0 kg crate rests on the floor of a warehouse. The coefficient of static friction is 0.750 and the coefficient of kinetic friction is 0.450. We want to determine the horizontal force (to the right) required just to start the crate moving. The horizontal force (to the right) required to just get the crate moving along the floor is', 4, 2),
(130, 'The magnetic field is', 2, 2),
(131, 'A standing wave is formed by waves of frequency 256 Hz. The speed of the waves is 128 m/s. The distance between the nodes must be', 3, 2),
(132, 'Consider the direction in which the magnetic field lines point for a bar magnet with a north pole and a south pole. That situation that would give the same pattern of lines is the', 3, 2),
(133, 'When a wave moves from shallow water to deep water, the', 3, 2),
(134, 'The definition of magnetic inclination is the angle between', 3, 2),
(135, 'A 15,000 Hz sound wave and a 5,000 Hz sound wave approach a small hole in a wall. The degree of â€œbendingâ€ as the waves move through the hole', 3, 2),
(136, 'The frequency of middle C is 256 Hz. The frequency of a C note two octaves below middle C is', 3, 2),
(137, 'An air column that is solid at one end is used to determine the speed of sound. The frequency of an E note tuning fork is 329.6 Hz. The length of the shortest air column producing the resonance is 25.0 cm. The speed of the sound must be', 4, 2),
(138, 'If the frequency of a â€œCâ€ note in the musical scale is 262 Hz, then we can say that the', 4, 2),
(139, 'When the two pulses interfere with each other, the result is', 3, 2),
(140, 'A string is plucked producing four loops (antinodes). The length of the string is 12.00 m. The wavelength of the wave must be', 4, 2),
(141, 'A wave travels at a speed of 10.0 cm/s in shallow water. The wavelength of the wave in the shallow water is 2.0 cm. The speed of the wave in the deep water is 12.0 cm/s. The wavelength of the wave in deep water is', 4, 2),
(142, 'The sound level in a room may be 50 dB. A sound that is twice as loud as a 50 dB sound is', 4, 2),
(143, 'If the frequency of one source of sound is 500 Hz and a second sound source is 504 Hz, then when the two waves interfere with each other, the beat frequency will be', 4, 2),
(144, 'For a tube closed at one end, the length of the tube at a frequency of 256 Hz will be', 3, 2),
(145, 'The nucleus of an atom consists of', 3, 4),
(146, 'The number of moles of solute present in 1 kg of a solvent is called its', 3, 4),
(147, 'The most electronegative element among the following is', 3, 4),
(148, 'The metal used to recover copper from a solution of copper sulphate is', 4, 4),
(149, 'The number of d-electrons in Fe2+ (Z = 26) is not equal to that of', 4, 4),
(150, 'The metallurgical process in which a metal is obtained in a fused state is called', 3, 4),
(151, 'The molecules of which gas have highest speed?', 4, 4),
(153, 'The law which states that the amount of gas dissolved in a liquid is proportional to its partial pressure is', 3, 4),
(154, '10. 	The main buffer system of the human blood is', 4, 4),
(156, 'The most commonly used bleaching agent is', 4, 4),
(157, 'The nucleus of a hydrogen atom consists of', 4, 4),
(158, 'he heat required to raise the temperature of body by 1 K is called', 3, 4),
(159, 'The nuclear particles which are assumed to hold the nucleons together are', 3, 4),
(160, 'The mass of P4O10 that will be obtained from the reaction of 1.33 gram of P4 and 5.07 of oxygen is', 4, 4),
(161, 'The octane number of zero is assigned to', 3, 4),
(162, 'The metal that is used as a catalyst in the hydrogenation of oils is', 3, 4),
(163, 'The most abundant rare gas in the atmosphere is', 3, 4),
(164, 'The Latin word formica means ant. The name formic acid is derived from this Latin word because', 4, 4),
(165, 'The ore which is found in abundance in India is', 3, 4),
(166, 'The inherited traits of an organism are controlled by', 3, 4),
(167, 'The heat energy produced when the human body metabolises 1 gram of fat is', 4, 4),
(168, 'What are the number of moles of CO2 which contains 16 g of oxygen?', 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `recommendation`
--

CREATE TABLE `recommendation` (
  `r_id` int(11) NOT NULL,
  `first_recommendation` varchar(100) NOT NULL,
  `second_recommendation` varchar(100) NOT NULL,
  `third_recommendation` varchar(100) NOT NULL,
  `marks_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `s_id` int(11) NOT NULL,
  `subject` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `related_field` varchar(100) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`s_id`, `subject`, `related_field`) VALUES
(1, 'Mathematics', ''),
(2, 'Physics', ''),
(3, 'Biology', ''),
(4, 'Chemistry', ''),
(5, 'Islamic studies', ''),
(6, 'History', ''),
(7, 'Geography', ''),
(8, 'General Science', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `birth_date` text NOT NULL,
  `school` varchar(100) NOT NULL,
  `email` text NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `first_name`, `last_name`, `gender`, `birth_date`, `school`, `email`, `password`) VALUES
(1, 'samira', 'ahmadzai', 'female', '1378/1/4', 'mahmoode- tarzi ', 'samiraahmadzai44@gmail.com', 'samira'),
(2, 'kawsar', 'ibrahimi', 'female', '1398/2/21', 'zarghona ana', 'kawsaribrahimi@gmail.com', 'kawsar'),
(3, 'jhgjh', 'jkzsgdjkl', 'male', '1367/2/3', 'jdfjk', 'sahiba@gmail.com', 'www'),
(4, 'ahmad', 'khan', 'male', '1367/2/3', 'nadi maloom', 'khapala@gmail.com', '1234'),
(5, 'wahidullah', 'mudaser', 'male', '1367/2/3', 'habibia high school', 'wahidullah.mdr@gmail.com', 'naeedars'),
(6, 'bal ', 'nafar', 'male', '1367/2/3', 'nadi maloom', 'sahiba@gmail.com', 'dsfs'),
(7, 'idriss', 'khan', 'male', '1367/2/3', 'nadi maloom', 'sahiba@gmail.com', '123'),
(8, 'kawsar', 'bendi', 'female', '1367/2/3', 'walaka', 'sahiba@gmail.com', '332211'),
(10, '', 'jhfhk', '', 'jhfhk', '', '', ''),
(11, '', '', '', '', '', '', ''),
(12, 'asad', 'fjhgkh', 'male', '1367/2/3', 'ddddddddd', 'sahiba@gmail.com', '31234'),
(13, 'sammi ', 'jksdhkla', 'male', '1367/2/3', 'nadi maloom', 'sahiba@gmail.com', 'sss'),
(14, 'wahidullah', 'nafar', 'male', '1367/2/3', 'walaka', 'wahidullah.mdr@gmail.com', 'swswsw');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_info`
--
ALTER TABLE `faculty_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faculty_id` (`faculty_id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `recommendation`
--
ALTER TABLE `recommendation`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=643;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `faculty_info`
--
ALTER TABLE `faculty_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `q_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT for table `recommendation`
--
ALTER TABLE `recommendation`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `faculty_info`
--
ALTER TABLE `faculty_info`
  ADD CONSTRAINT `faculty_info_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
