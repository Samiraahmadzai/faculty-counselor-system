# Faculty Counselor System

Faculty Counselor System
A project for school graduated students to choose a proper faculty. 

**Members**

Samira Ahmadzai, Kawsar Ibrahimi

**Motivation**

School graduated student face much problems in choosing faculty.
They have less or even no information about university system and any faculty.
They doesn’t know their skill and interest field.
They give entrance exam unconsciously, a few of them pass to the faculties that they planned for their selves. Others fail, because of improper selection.
Some of them pass and study the faculty they don’t  like, even they didn’t hear that’s name yet. 

**Objectives**

Enable students to get information about university and faculties.
Enable students to test themselves. (For knowing their skills and interest)
To facilitate for them an easy way of faculty selection.
Finally students able to get guidance about how to reach to their academic goals.

